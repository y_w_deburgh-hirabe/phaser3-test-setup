export class Background extends Phaser.GameObjects.Container {

    constructor(scene: Phaser.Scene) {
        super(scene);
        
        this.scene.add.image(0, 0, 'backdrop')
        .setOrigin(0);
        
    }
}