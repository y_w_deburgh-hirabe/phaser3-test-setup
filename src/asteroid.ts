export class Asteroid extends Phaser.Physics.Arcade.Sprite
{
    constructor (scene, x, y)
    {
        super(scene, x, y, 'asteroid');
    }

    spawn ()
    {
        var x;
        var y;
        // Chooses a random edge to spawn asteroid
        switch (Phaser.Math.Between(0, 3)) {
            case 0: {
                x = -50;
                y = Phaser.Math.Between(0, 600);
                break;
            }
            case 1: {
                x = 850;
                y = Phaser.Math.Between(0, 600);
                break;
            }
            case 2: {
                x = Phaser.Math.Between(0, 800);
                y = -50;
                break;
            }
            case 3: {
                x = Phaser.Math.Between(0, 800);
                y = 650;
                break;
            }
        }

        this.body.reset(x, y);
        this.setActive(true);
        this.setVisible(true);
        
        // Generates random velocity and direction, based on random size
        var direction = Phaser.Math.Between(1, 360);
        var sizeMod = Phaser.Math.FloatBetween(0.5, 1.5);

        var rotVelocity = Phaser.Math.Between(-100, 100) / sizeMod;
        var velocity = Phaser.Math.Between(100, 200) / sizeMod;


        this.setDisplaySize(100 * sizeMod, 100 * sizeMod);

        this.rotation = direction;
        this.setAngularVelocity(rotVelocity);
        this.scene.physics.velocityFromRotation(direction, velocity, this.body.velocity);
    }

    preUpdate (time, delta)
    {
        super.preUpdate(time, delta);
        // Kill out of bounds asteroid
        if (this.y <= -150 || this.y >= 750 || this.x <= -150 || this.x >= 950)
        {
            this.setActive(false);
            this.setVisible(false);
        }
    }
}

export class Asteroids extends Phaser.Physics.Arcade.Group
{
    constructor (scene)
    {
        super(scene.physics.world, scene);

        this.createMultiple({
            frameQuantity: 50,
            key: 'asteroid',
            active: false,
            visible: false,
            classType: Asteroid
        });
    }

    spawnAsteroid ()
    {
        // Spawns a random asteroid
        let asteroid = this.getFirstDead(false);

        if (asteroid)
        {
            asteroid.spawn();
        }
    }
}