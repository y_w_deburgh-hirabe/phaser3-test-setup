export class Bullet extends Phaser.Physics.Arcade.Sprite
{
    constructor (scene, x, y)
    {
        super(scene, x, y, 'bullet');
    }

    fire (x, y, direction)
    {

        this.body.reset(x, y);

        this.setActive(true);
        this.setVisible(true);

        this.rotation = direction;
        this.scene.physics.velocityFromRotation(direction, 600, this.body.velocity);
    }

    preUpdate (time, delta)
    {
        super.preUpdate(time, delta);

        if (this.y <= -32 || this.y >= 632 || this.x <= -32 || this.x >= 832)
        {
            this.setActive(false);
            this.setVisible(false);
        }
    }
}

export class Bullets extends Phaser.Physics.Arcade.Group
{
    constructor (scene)
    {
        super(scene.physics.world, scene);

        this.createMultiple({
            frameQuantity: 5,
            key: 'bullet',
            active: false,
            visible: false,
            classType: Bullet
        });
    }

    fireBullet (x, y, direction)
    {
        let bullet = this.getFirstDead(false);

        if (bullet)
        {
            bullet.fire(x, y, direction);
        }
    }
}