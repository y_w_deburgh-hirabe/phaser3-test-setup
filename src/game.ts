import 'phaser';
import GameScene from './scene';

const config: Phaser.Types.Core.GameConfig = {
    type: Phaser.AUTO,
    backgroundColor: '#125555',
    width: 800,
    height: 600,
    scene: GameScene,
    physics: {
        default:'arcade',
        arcade: {
            debug: false,
            fps: 155,
            gravity: { y: 0 }
        }
    }
};

new Phaser.Game(config);