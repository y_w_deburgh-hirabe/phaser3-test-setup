import { Background } from "./background";
import { Asteroids } from "./asteroid";
import { Bullets } from "./bullet";



// Declare variables
var sprite;

var leftKey;
var rightKey;
var accelKey;
var shootKey;

var bullets;
var asteroids;

var timedAsteroidEvent;

var score;
var lives;

var lifeText;
var scoreText;
var endText;
var portrait;

export default class gameScene extends Phaser.Scene 
    {    
    constructor () {
        super('game');
    }

    preload() 
    {
        // Load sprites and background
        this.load.image('ship', 'assets/character.png');
        this.load.image('bullet', 'assets/bullet.png');
        this.load.image('asteroid', 'assets/asteroid.png');

        this.load.image('face0',  'assets/face0.png')
        this.load.image('face1',  'assets/face1.png')
        this.load.image('face2',  'assets/face2.png')

        this.load.image('backdrop', 'assets/space.jpg');
    }

    create() 
    {
        const bg = new Background(this);
        this.add.existing(bg);

        // Timer for spawning new asteroids
        timedAsteroidEvent = this.time.addEvent({ delay: 1000, callback: this.asteroidSpawnEvent, callbackScope: this});
        score = 0;
        lives = 3;


        // Text
        lifeText = this.add.text(20, 20, 'Lives: ',  {fontSize: '40px', color: '#00ff00' })
        .setDepth(1);

        scoreText = this.add.text(20, 50, 'Score: ',  {fontSize: '40px', color: '#ffffff' })
        .setDepth(1);

        endText = this.add.text(400, 300, 'Game Over!', {fontSize: '64px', color: '#ff0000' })
        .setOrigin(0.5)
        .setDepth(1)
        .setVisible(false);
        
        // Portrait
        portrait = this.add.image(20, 85, 'face0').setDisplaySize(100, 100);
        portrait.setDepth(1);
        portrait.setOrigin(0);

        // Create groups for bullets and asteroids
        bullets = new Bullets(this);
        asteroids = new Asteroids(this);


        // Ship physics
        sprite = this.physics.add.image(320, 240, 'ship').setDisplaySize(60, 47);
        sprite.setDamping(true);
        sprite.setDrag(0.99);
        sprite.setMaxVelocity(200);

        // Assign movement keys (WASD + spacebar)
        accelKey = this.input.keyboard.addKey('UP');
        leftKey = this.input.keyboard.addKey('LEFT');
        rightKey = this.input.keyboard.addKey('RIGHT');
        shootKey =   this.input.keyboard.addKey('SPACE');

        // Check collision with ship and asteroid and bullets
        this.physics.add.overlap(sprite, asteroids, this.collideAsteroid, null, this);
        this.physics.add.overlap(bullets, asteroids, this.shotAsteroid, null, this);
    }

    update (delta)
    {
        this.controlShip();

        this.updateHUD();

        if (lives <= 0) {
            // Pause the scene on the game over screen
            endText.setVisible(true);
            this.scene.pause();
            
        }
    }


    controlShip ()
    {
         // Movement
        if (accelKey.isDown) {
            this.physics.velocityFromRotation(sprite.rotation, 250, sprite.body.acceleration);
        } else {
            sprite.setAcceleration(0);
        }
        if (leftKey.isDown) {
            sprite.setAngularVelocity(-300);
        } else if (rightKey.isDown) {
            sprite.setAngularVelocity(300);
        } else {
            sprite.setAngularVelocity(0);
        }
        this.physics.world.wrap(sprite, 32);


        // Shoot
        if (Phaser.Input.Keyboard.JustDown(shootKey))
        {
           bullets.fireBullet(sprite.x, sprite.y, sprite.rotation);
        }

    }


    asteroidSpawnEvent ()
    {
        // Repeating event that spawns an asteroid and increases score. Delay between asteroids slowly decreases
        asteroids.spawnAsteroid();
        var maxDelay;

        if (score > 650) {
            maxDelay = 100;
        } else {
            maxDelay = 750 - score;
        }
        var spawnDelay = Phaser.Math.Between(50, maxDelay)
        timedAsteroidEvent.reset({ delay: spawnDelay, callback: this.asteroidSpawnEvent, callbackScope: this, repeat: 1});

        score++;
    }


    collideAsteroid (sprite, asteroid)
    {
        // Called when ship collides with an asteroid
        // Sends out of bounds to be killed
        asteroid.x = 10000;
        lives--;
    }


    shotAsteroid (bullet, asteroid)
    {
        // Called when a bullet collides with an asteroid
        // Sends out of bounds to be killed
        bullet.x = 10000;
        asteroid.x = 10000;
    }


    updateHUD ()
    {
        lifeText.setText("Lives: " + lives);
        // Update character portrait and text colour depending on lives
        if (lives <= 1) {
            lifeText.setColor('#ff0000');
            portrait.setTexture('face2');
        } else if (lives <= 2) {
            lifeText.setColor('#ffff00');
            portrait.setTexture('face1');
        } else {
            lifeText.setColor('#00ff00');
            portrait.setTexture('face0');
        }
        scoreText.setText("Score: " + score);
    }
}